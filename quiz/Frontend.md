## **Question 1:**

How would you speed up a single page application?

## **Question 2:**

Explain the `this` keyword and it's quirks in JavaScript

## **Question 3:**

What is the difference between border-box and content-box?

## **Question 4:**

What's the difference between == and === operators?

## **Question 5:**

What is accessibility? How do you achieve it?

## **Question 6:**

What is the difference between session storage, local storage and cookies?

## **Question 7:**

How does hoisting work in JavaScript, and what is the order of hoisting?

## **Question 8:**

What is the difference in usage of callback, promise, and async/await?