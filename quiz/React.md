## **Question 1:**

Why us React over other JS frameworks?

## **Question 2:**

What happens during the lifecycle of a React component?

## **Question 3:**

What are stateless components?

## **Question 4:**

Differentiate between Real DOM and Virtual DOM.

## **Question 5:**

How would you improve SEO of a react Application?

## **Question 6:**

What are the limitations of React?

## **Question 7:**

What are some advantages of using React?

## **Question 8:**

What is server-side rendering, and what problems does it solve?

## **Question 9:**

List and briefly describe some security attacks on the frontend?.