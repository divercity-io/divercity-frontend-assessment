# **Divercity SWE Challenge**

Thanks for taking the time to complete this exercise. We are excited that you are considering joining Divercity.

In this test, you are expected to write a small web application that will serve as a submission form for future interested interns.

The focus will be on usability and design fidelity.

These are the two sections of the form.

![](./images/form.png)

The form should be fully functional and ready for submission. Leave a placeholder in your code the API url.

When submitted, there should be a loader that pops into the frame.

![](./images/loading.gif)

---
The expected responses from the API are as follows.

```javascript
    {
        message: 'Successfully submitted' | ' There was a server error.'
        error: false | true
    }
```

---

Please use the design to interpret the specific requirements. The form should have:

- validation across all input fields
- visual cues for incorrect input
- state for buttons (image below.)
- saved the state between the two sections of the form (I should be able to go back to page 1 and have my information saved.)


![](./images/buttonStates.png)

---

## Implementation

This web app must be developed using [next.js](https://nextjs.org) using [tailwind.css](tailwindcss.com) as the frameworks.

Each component you make should be reusable and accept different props to maximize its reusability.

### Notes and recommendations
* The project structure is up to you to decide
* You are recommended to use git commits along with commenting logically to demonstrate the development progress
* Writing tests and adhering to development standards/conventions leaves a positive impression
* Not everything is spelled out for you. That was done purposely. Please follow best practices to ensure the usability of the form and reusability of your components.

## **Submission :**

- Fork the project into a private bitbucket repo
- Complete the tasks in steps and merge your pull requests(with adequate descriptions) to main
- **Add gil@divercity.io to the repo so we can clone & review your code**
- Email your recruiter once finished and ready for the review

## **Bonus :**
- Bonus: Deploy your application to an AWS Account
- Bonus: Implement the loader in canvas
- Bonus: Add animation transition to button states (default, hover, disabled).
